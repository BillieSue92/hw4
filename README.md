# ---------------------------------------------------- #
# File: HW4 Pseudocode.txt
# ---------------------------------------------------- #
# Author(s): Crystal Liu, Ivy McGinley
# BitBucket - user: BillieSue92
# ---------------------------------------------------- #
# Plaftorm:    Windows
# Environment: Python 2.7.7 :Anaconda 2.0.1
# Libaries:    Tkinter
# ---------------------------------------------------- #
# Description: Two-player game of Pong
#			   Player 1 keys: w,s
#			   Player 2 keys: o,l
#			   Quit game: q
# ---------------------------------------------------- #


# create class Application

	# create initial parameters

	# initalize Frame

	# initializing pong game function

		# create objects
			# call create arena function
			# call create paddles function
			# call create ball function

		# window title  

		# call player controls

		# score-keeper window

		# call update function

	# create arena function

	# create paddles function

	# create ball function

	# game controls function

		# if 'w' is pressed:
			# move left paddle up
		# if 's' is pressed:
			# move left paddle down
		# if 'o' is pressed:
			# move right paddle up
		# if 'l' is pressed:
			# move right paddle down
			
		# if 'q' is pressed:
			# quit game

	# function that checks if ball has hit paddles

	# update positions of paddles and ball
		# move ball to new position

		# if ball hits top wall
			# reflect
		# if ball hits bottom wall
			# reflect
		# if ball hits a paddle
			# reflect
		# if ball enters player 1's goal:
			# add 1 point to player 2
		# if ball enters player 2's goal:
			# add 1 point to player 1

		# repeat entire process


# begin application 

#####END##### 