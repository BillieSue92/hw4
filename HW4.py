# ---------------------------------------------------- #
# File: HW4.py
# ---------------------------------------------------- #
# Author(s): Crystal Liu, Ivy McGinley
# BitBucket - user: BillieSue92
# ---------------------------------------------------- #
# Plaftorm:    Windows
# Environment: Python 2.7.7 :Anaconda 2.0.1
# Libaries:    Tkinter
# ---------------------------------------------------- #
# Description: Two-player game of Pong
#			   Player 1 keys: w,s
#			   Player 2 keys: o,l
#			   Quit game: q
# ---------------------------------------------------- #


#----------------------------------------------------------------#

#						IMPORTED MODULES                         #

#----------------------------------------------------------------#

import Tkinter as tk
from Tkinter import Frame, BOTH, Canvas

#----------------------------------------------------------------#

#							Pong Game                            #

#----------------------------------------------------------------#

class Application(Frame):

	# inital parameters
	player1 = 0
	player2 = 0
	winWIDTH = 575
	winHEIGHT = 300
	paddle1X = 2
	paddle1Y = winHEIGHT/2
	paddle2X = winWIDTH
	paddle2Y = winHEIGHT/2
	paddleHalfLength = 25
	paddleWidth = 10	
	ballX = winWIDTH/2
	ballY = winHEIGHT/2
	ballDX = 3
	ballDY = -3
	ballRadius = 5
	paddleSpeed = 15
	ballSpeed = 2
	player1Points = 0
	player2Points = 0


	# initalize Frame
	def __init__(self, master):
		Frame.__init__(self, master)

		self.master = master

		self.pack() 
		self.initUI()

    # initializing pong game
	def initUI(self):

		# create objects
		self.CreateArena()
		self.CreatePaddles()
		self.CreateBall()

		# window title
		self.master.title("The Good Ol' Game of Pong")   

		# players controls
		self.master.bind("<Key>", self.Keys)

		# score-keeper
		self.textLabel = self.canvas.create_text(self.winWIDTH/2,10,
			text=str(self.player1Points)+" | "+str(self.player2Points))

		# continual update function
		self.after(200,self.doMove)

	# create the arena - origin at top left corner
	def CreateArena(self):

		self.canvas = Canvas(self, width = self.winWIDTH, height = self.winHEIGHT)
		self.canvas.pack()

	# create the paddles
	def CreatePaddles(self):

		self.paddle1 = self.canvas.create_rectangle(self.paddle1X, self.paddle1Y - self.paddleHalfLength,
			self.paddle1X + self.paddleWidth, self.paddle1Y + self.paddleHalfLength, outline="#f50", fill="#f50")
		self.paddle2 = self.canvas.create_rectangle(self.paddle2X - self.paddleWidth,
			self.paddle2Y - self.paddleHalfLength, self.paddle2X, self.paddle2Y + self.paddleHalfLength,
			outline="#05f", fill="#05f")

	# create ball
	def CreateBall(self):
		self.ball = self.canvas.create_oval(self.ballX - self.ballRadius, self.ballY - self.ballRadius,
			self.ballX + self.ballRadius, self.ballY + self.ballRadius, outline="black", fill="green", width=1)

	# game controls
	def Keys(self, event):

		if event.char == 'w':
			if self.canvas.coords(self.paddle1)[1]>=self.paddleSpeed/2:
				self.canvas.move(self.paddle1,0,-self.paddleSpeed)
		if event.char == 's':
			if self.canvas.coords(self.paddle1)[3]<=self.winHEIGHT - self.paddleSpeed/2:
				self.canvas.move(self.paddle1,0,self.paddleSpeed)

		if event.char == 'o':
			if self.canvas.coords(self.paddle2)[1]>=self.paddleSpeed/2:
				self.canvas.move(self.paddle2,0,-self.paddleSpeed)
		if event.char == 'l':
			if self.canvas.coords(self.paddle2)[3]<=self.winHEIGHT - self.paddleSpeed/2:
				self.canvas.move(self.paddle2,0,self.paddleSpeed)

		# quit game			
		if event.char == 'q':
			self.master.destroy()

	# checking if ball hits paddles - code taken from "Caleb Robinson"
	def doCollide(self,coords1,coords2):
		height1 = coords1[3]-coords1[1]
		width1 = coords1[2]-coords1[0]
		height2 = coords2[3]-coords2[1]
		width2 = coords2[2]-coords2[0]
		return not (coords1[0] + width1 < coords2[0] or coords1[1] + height1 < coords2[1] or coords1[0] > coords2[0] + width2 or coords1[1] > coords2[1] + height2)

	# update positions of paddles and ball - code taken from "Caleb Robinson"
	def doMove(self):
		self.canvas.move(self.ball,self.ballDX, self.ballDY)
		if self.canvas.coords(self.ball)[1] <= 0:
			self.ballDY = -self.ballDY
		if self.canvas.coords(self.ball)[3] >= self.winHEIGHT:
			self.ballDY = -self.ballDY
		if self.doCollide(self.canvas.coords(self.ball),self.canvas.coords(self.paddle1)) or self.doCollide(self.canvas.coords(self.ball),self.canvas.coords(self.paddle2)):
			self.ballDX = -self.ballDX
		if self.canvas.coords(self.ball)[0] <= 0:
			self.ballDX = -self.ballDX
			self.player2Points+=1
			self.canvas.delete(self.textLabel)
			self.textLabel = self.canvas.create_text(self.winWIDTH/2,10, text=str(self.player1Points)+" | "+str(self.player2Points))
			self.canvas.coords(self.ball,self.winWIDTH/2,self.winHEIGHT/2,self.winWIDTH/2+10,self.winHEIGHT/2+10)
		if self.canvas.coords(self.ball)[2] >= self.winWIDTH:
			self.ballDX = -self.ballDX
			self.player1Points+=1
			self.canvas.delete(self.textLabel)
			self.textLabel = self.canvas.create_text(self.winWIDTH/2,10, text=str(self.player1Points)+" | "+str(self.player2Points))
			self.canvas.coords(self.ball,self.winWIDTH/2,self.winHEIGHT/2,self.winWIDTH/2+10,self.winHEIGHT/2+10)
		self.after(10, self.doMove)

def main():

	root = tk.Tk()
	app = Application(root)
	app.mainloop()  
 
if __name__ == '__main__':
    main()  